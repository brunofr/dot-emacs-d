;;;;;;;;;;;;;;;;;;;;
;; set up unicode
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

;; Save desktop
(desktop-save-mode t)

;; default to unified diffs
(setq diff-switches "-u")

;; Markdown mode
(setq markdown-command "~/.emacs.d/markdown.pl | ~/.emacs.d/SmartyPants.pl")
(add-to-list 'auto-mode-alist '("\\.txt" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md" . markdown-mode))
(setq markdown-css-path  (substitute-in-file-name "$HOME/.emacs.d/main.css"))

;; Fullscreen mode on Cocoa

(if (fboundp 'ns-toggle-fullscreen)
    (global-set-key [(meta return)] 'ns-toggle-fullscreen))

;;(when window-system (color-theme-solarized-dark))

;; fix the PATH variable
(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (shell-command-to-string "$SHELL -i -c 'echo $PATH'")))
  (setenv "PATH" path-from-shell)
  (setq exec-path (split-string path-from-shell path-separator))))

(if window-system (set-exec-path-from-shell-PATH))

(setq make-backup-files nil)
(setq auto-save-default nil)
(setq-default tab-width 2)
(setq-default indent-tabs-mode nil)
(setq inhibit-startup-message t)

(fset 'yes-or-no-p 'y-or-n-p)

(delete-selection-mode t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(blink-cursor-mode t)
(show-paren-mode t)
(column-number-mode t)
(set-fringe-style -1)
(tooltip-mode -1)
(global-linum-mode 1)
(setq linum-format " %d  ")
(menu-bar-mode nil)
(setq speedbar-show-unknown-files t)

(set-frame-font "Menlo-12")
;;(load-theme 'wheatgrass)
;;(load-theme 'tango-dark)

;; rebind the shift-up xterm event to S-up since
;; emacs thinks it's <select>
(if (equal "xterm" (tty-type))
    (define-key input-decode-map "\e[1;2A" [S-up]))

(windmove-default-keybindings)

;; setup alternate keyboard mappings
;; leave left alt as dead key for special chars
(setq ns-alternate-modifier (quote meta))
;; use right alt as meta
(setq ns-right-alternate-modifier nil)

;; golang

(require 'go-mode-load)
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/brunofr/ac-dict")
(ac-config-default)
(require 'go-autocomplete)
(require 'auto-complete-config)
(require 'protobuf-mode)
(add-to-list 'auto-mode-alist '("\\.proto" . protobuf-mode))

;;; init.el ends here

